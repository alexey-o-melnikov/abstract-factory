package factory;

import factory.products.AppleDevice;
import factory.products.BlackIPad;
import factory.products.GoldIPad;
import factory.products.WhiteIPad;

public class IPadFactory extends DeviceColorFactory  {
    public AppleDevice createWhite() {
        return new WhiteIPad();
    }

    public AppleDevice createBlack() {
        return new BlackIPad();
    }

    public AppleDevice createGold() {
        return new GoldIPad();
    }
}