package factory;

import factory.products.AppleDevice;
import factory.products.BlackIPhone;
import factory.products.GoldIPhone;
import factory.products.WhiteIPhone;

public class IPhoneFactory extends DeviceColorFactory  {
    public AppleDevice createWhite() {
        return new WhiteIPhone();
    }

    public AppleDevice createBlack() {
        return new BlackIPhone();
    }

    public AppleDevice createGold() {
        return new GoldIPhone();
    }
}