package factory;

import factory.products.AppleDevice;

import java.util.HashMap;
import java.util.Map;

public class AppleDeviceFactory {
    Map<String, DeviceColorFactory > appleDevices = new HashMap<>();

    public AppleDeviceFactory() {
        appleDevices.put("iPhone", new IPhoneFactory());
        appleDevices.put("iPad", new IPadFactory());
        appleDevices.put("iPod", new IPodFactory());
    }

    public AppleDevice getAppleDevice(String type, String color) {
        DeviceColorFactory colorFactory = appleDevices.get(type);
        if(colorFactory != null) {
            return colorFactory.getAppleDevice(color);
        } else {
            return null;
        }
    }
}