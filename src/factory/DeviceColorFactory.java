package factory;

import factory.products.AppleDevice;

import java.util.HashMap;
import java.util.Map;

public abstract class DeviceColorFactory {
    Map<String, AppleDevice> deviceColors = new HashMap<>();

    public abstract AppleDevice createWhite();
    public abstract AppleDevice createBlack();
    public abstract AppleDevice createGold();

    public DeviceColorFactory() {
        deviceColors.put("white", createWhite());
        deviceColors.put("black", createBlack());
        deviceColors.put("gold", createGold());
    }

    public AppleDevice getAppleDevice(String color) {
        return deviceColors.get(color);
    }
}