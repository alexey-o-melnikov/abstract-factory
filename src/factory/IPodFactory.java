package factory;

import factory.products.AppleDevice;
import factory.products.BlackIPod;
import factory.products.GoldIPod;
import factory.products.WhiteIPod;

public class IPodFactory extends DeviceColorFactory  {
    public AppleDevice createWhite() {
        return new WhiteIPod();
    }

    public AppleDevice createBlack() {
        return new BlackIPod();
    }

    public AppleDevice createGold() {
        return new GoldIPod();
    }
}