package factory.products;

/**
 *
 */
public abstract class AppleDevice {
    @Override
    public String toString() {
        return this.getClass().getName();
    }
}
