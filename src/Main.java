import factory.AppleDeviceFactory;
import factory.products.AppleDevice;

public class Main {

    public static void main(String[] args) {
        AppleDeviceFactory appleDeviceFactory = new AppleDeviceFactory();
        AppleDevice device = appleDeviceFactory.getAppleDevice("iPhone", "black");

        if (device != null) {
            System.out.println(device.toString());
        }
    }
}
